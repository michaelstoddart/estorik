<?php

namespace App\Entity;

use App\Repository\ExcelDataRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ExcelDataRepository::class)]
class ExcelData
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $CompteAffaire;

    #[ORM\Column(type: 'string', length: 255)]
    private $CompteEvenement;

    #[ORM\Column(type: 'string', length: 255)]
    private $CompteDernierEvenement;

    #[ORM\Column(type: 'string', length: 255)]
    private $NumeroDeFichier;

    #[ORM\Column(type: 'string', length: 255)]
    private $LibelleCivilite;

    #[ORM\Column(type: 'string', length: 255)]
    private $ProprietaireActuelDuVehicule;

    #[ORM\Column(type: 'string', length: 255)]
    private $Nom;

    #[ORM\Column(type: 'string', length: 255)]
    private $Prenom;

    #[ORM\Column(type: 'string', length: 255)]
    private $NoEtNomDeLaRue;

    #[ORM\Column(type: 'string', length: 255)]
    private $ComplementAdresse1;

    #[ORM\Column(type: 'string', length: 255)]
    private $CodePostal;

    #[ORM\Column(type: 'string', length: 255)]
    private $Ville;

    #[ORM\Column(type: 'string', length: 255)]
    private $Telephone;

    #[ORM\Column(type: 'string', length: 255)]
    private $TelephonePortable;

    #[ORM\Column(type: 'string', length: 255)]
    private $TelephoneJob;

    #[ORM\Column(type: 'string', length: 255)]
    private $Email;

    #[ORM\Column(type: 'string', length: 255)]
    private $DateDeMiseEnCirculation;

    #[ORM\Column(type: 'string', length: 255)]
    private $DateAchat;

    #[ORM\Column(type: 'string', length: 255)]
    private $DateDernierEvenement;

    #[ORM\Column(type: 'string', length: 255)]
    private $LibelleMarque;

    #[ORM\Column(type: 'string', length: 255)]
    private $LibelleModele;

    #[ORM\Column(type: 'string', length: 255)]
    private $Version;

    #[ORM\Column(type: 'string', length: 255)]
    private $VIN;

    #[ORM\Column(type: 'string', length: 255)]
    private $Immatriculation;

    #[ORM\Column(type: 'string', length: 255)]
    private $TypeDeProspect;

    #[ORM\Column(type: 'string', length: 255)]
    private $Kilometrage;

    #[ORM\Column(type: 'string', length: 255)]
    private $LibelleEnergie;

    #[ORM\Column(type: 'string', length: 255)]
    private $VendeurVN;

    #[ORM\Column(type: 'string', length: 255)]
    private $VendeurVO;

    #[ORM\Column(type: 'string', length: 255)]
    private $CommentaireDeFacturation;

    #[ORM\Column(type: 'string', length: 255)]
    private $TypeVNVO;

    #[ORM\Column(type: 'string', length: 255)]
    private $NoDeDossierVN;

    #[ORM\Column(type: 'string', length: 255)]
    private $IntermediareDeVente;

    #[ORM\Column(type: 'string', length: 255)]
    private $DateEvenement;

    #[ORM\Column(type: 'string', length: 255)]
    private $OrigineEvenement;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompteAffaire(): ?string
    {
        return $this->CompteAffaire;
    }

    public function setCompteAffaire(string $CompteAffaire): self
    {
        $this->CompteAffaire = $CompteAffaire;

        return $this;
    }

    public function getCompteEvenement(): ?string
    {
        return $this->CompteEvenement;
    }

    public function setCompteEvenement(string $CompteEvenement): self
    {
        $this->CompteEvenement = $CompteEvenement;

        return $this;
    }

    public function getCompteDernierEvenement(): ?string
    {
        return $this->CompteDernierEvenement;
    }

    public function setCompteDernierEvenement(string $CompteDernierEvenement): self
    {
        $this->CompteDernierEvenement = $CompteDernierEvenement;

        return $this;
    }

    public function getNumeroDeFichier(): ?string
    {
        return $this->NumeroDeFichier;
    }

    public function setNumeroDeFichier(string $NumeroDeFichier): self
    {
        $this->NumeroDeFichier = $NumeroDeFichier;

        return $this;
    }

    public function getLibelleCivilite(): ?string
    {
        return $this->LibelleCivilite;
    }

    public function setLibelleCivilite(string $LibelleCivilite): self
    {
        $this->LibelleCivilite = $LibelleCivilite;

        return $this;
    }

    public function getProprietaireActuelDuVehicule(): ?string
    {
        return $this->ProprietaireActuelDuVehicule;
    }

    public function setProprietaireActuelDuVehicule(string $ProprietaireActuelDuVehicule): self
    {
        $this->ProprietaireActuelDuVehicule = $ProprietaireActuelDuVehicule;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->Prenom;
    }

    public function setPrenom(string $Prenom): self
    {
        $this->Prenom = $Prenom;

        return $this;
    }

    public function getNoEtNomDeLaRue(): ?string
    {
        return $this->NoEtNomDeLaRue;
    }

    public function setNoEtNomDeLaRue(string $NoEtNomDeLaRue): self
    {
        $this->NoEtNomDeLaRue = $NoEtNomDeLaRue;

        return $this;
    }

    public function getComplementAdresse1(): ?string
    {
        return $this->ComplementAdresse1;
    }

    public function setComplementAdresse1(string $ComplementAdresse1): self
    {
        $this->ComplementAdresse1 = $ComplementAdresse1;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->CodePostal;
    }

    public function setCodePostal(string $CodePostal): self
    {
        $this->CodePostal = $CodePostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->Ville;
    }

    public function setVille(string $Ville): self
    {
        $this->Ville = $Ville;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->Telephone;
    }

    public function setTelephone(string $Telephone): self
    {
        $this->Telephone = $Telephone;

        return $this;
    }

    public function getTelephonePortable(): ?string
    {
        return $this->TelephonePortable;
    }

    public function setTelephonePortable(string $TelephonePortable): self
    {
        $this->TelephonePortable = $TelephonePortable;

        return $this;
    }

    public function getTelephoneJob(): ?string
    {
        return $this->TelephoneJob;
    }

    public function setTelephoneJob(string $TelephoneJob): self
    {
        $this->TelephoneJob = $TelephoneJob;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->Email;
    }

    public function setEmail(string $Email): self
    {
        $this->Email = $Email;

        return $this;
    }

    public function getDateDeMiseEnCirculation(): ?string
    {
        return $this->DateDeMiseEnCirculation;
    }

    public function setDateDeMiseEnCirculation(string $DateDeMiseEnCirculation): self
    {
        $this->DateDeMiseEnCirculation = $DateDeMiseEnCirculation;

        return $this;
    }

    public function getDateAchat(): ?string
    {
        return $this->DateAchat;
    }

    public function setDateAchat(string $DateAchat): self
    {
        $this->DateAchat = $DateAchat;

        return $this;
    }

    public function getDateDernierEvenement(): ?string
    {
        return $this->DateDernierEvenement;
    }

    public function setDateDernierEvenement(string $DateDernierEvenement): self
    {
        $this->DateDernierEvenement = $DateDernierEvenement;

        return $this;
    }

    public function getLibelleMarque(): ?string
    {
        return $this->LibelleMarque;
    }

    public function setLibelleMarque(string $LibelleMarque): self
    {
        $this->LibelleMarque = $LibelleMarque;

        return $this;
    }

    public function getLibelleModele(): ?string
    {
        return $this->LibelleModele;
    }

    public function setLibelleModele(string $LibelleModele): self
    {
        $this->LibelleModele = $LibelleModele;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->Version;
    }

    public function setVersion(string $Version): self
    {
        $this->Version = $Version;

        return $this;
    }

    public function getVIN(): ?string
    {
        return $this->VIN;
    }

    public function setVIN(string $VIN): self
    {
        $this->VIN = $VIN;

        return $this;
    }

    public function getImmatriculation(): ?string
    {
        return $this->Immatriculation;
    }

    public function setImmatriculation(string $Immatriculation): self
    {
        $this->Immatriculation = $Immatriculation;

        return $this;
    }

    public function getTypeDeProspect(): ?string
    {
        return $this->TypeDeProspect;
    }

    public function setTypeDeProspect(string $TypeDeProspect): self
    {
        $this->TypeDeProspect = $TypeDeProspect;

        return $this;
    }

    public function getKilometrage(): ?string
    {
        return $this->Kilometrage;
    }

    public function setKilometrage(string $Kilometrage): self
    {
        $this->Kilometrage = $Kilometrage;

        return $this;
    }

    public function getLibelleEnergie(): ?string
    {
        return $this->LibelleEnergie;
    }

    public function setLibelleEnergie(string $LibelleEnergie): self
    {
        $this->LibelleEnergie = $LibelleEnergie;

        return $this;
    }

    public function getVendeurVN(): ?string
    {
        return $this->VendeurVN;
    }

    public function setVendeurVN(string $VendeurVN): self
    {
        $this->VendeurVN = $VendeurVN;

        return $this;
    }

    public function getVendeurVO(): ?string
    {
        return $this->VendeurVO;
    }

    public function setVendeurVO(string $VendeurVO): self
    {
        $this->VendeurVO = $VendeurVO;

        return $this;
    }

    public function getCommentaireDeFacturation(): ?string
    {
        return $this->CommentaireDeFacturation;
    }

    public function setCommentaireDeFacturation(string $CommentaireDeFacturation): self
    {
        $this->CommentaireDeFacturation = $CommentaireDeFacturation;

        return $this;
    }

    public function getTypeVNVO(): ?string
    {
        return $this->TypeVNVO;
    }

    public function setTypeVNVO(string $TypeVNVO): self
    {
        $this->TypeVNVO = $TypeVNVO;

        return $this;
    }

    public function getNoDeDossierVN(): ?string
    {
        return $this->NoDeDossierVN;
    }

    public function setNoDeDossierVN(string $NoDeDossierVN): self
    {
        $this->NoDeDossierVN = $NoDeDossierVN;

        return $this;
    }

    public function getIntermediareDeVente(): ?string
    {
        return $this->IntermediareDeVente;
    }

    public function setIntermediareDeVente(string $IntermediareDeVente): self
    {
        $this->IntermediareDeVente = $IntermediareDeVente;

        return $this;
    }

    public function getDateEvenement(): ?string
    {
        return $this->DateEvenement;
    }

    public function setDateEvenement(string $DateEvenement): self
    {
        $this->DateEvenement = $DateEvenement;

        return $this;
    }

    public function getOrigineEvenement(): ?string
    {
        return $this->OrigineEvenement;
    }

    public function setOrigineEvenement(string $OrigineEvenement): self
    {
        $this->OrigineEvenement = $OrigineEvenement;

        return $this;
    }
}
