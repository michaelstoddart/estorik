<?php

namespace App\Controller;

use App\Entity\ExcelData;
use App\Entity\FileModel;
use App\Form\FileModelType;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class ImportController extends AbstractController
{

    #[Route('/', name: 'app_home')]
    public function home(ManagerRegistry $doctrine, Request $request): Response
    {
        return $this->render('home/index.html.twig');
    }


    #[Route('/import', name: 'app_import')]
    public function index(ManagerRegistry $doctrine, Request $request): Response
    {
        $entityManager = $doctrine->getManager();
        $file = new FileModel();

        $form = $this->createForm(FileModelType::class, $file, ['label' => 'Import File']);


        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->get('filename')->getData();
           
            $fileFolder = __DIR__ . '/../../public/uploads/';  //choose the folder in which the uploaded file will be stored

            $filePathName = md5(uniqid()) . $file->getClientOriginalName();
                    try {
                        $file->move($fileFolder, $filePathName);
                    } catch (FileException $e) {
                        dd($e);
                    }
            $spreadsheet = IOFactory::load($fileFolder . $filePathName); 
            $row = $spreadsheet->getActiveSheet()->removeRow(1); // remove the first file line 
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true); 
            foreach($sheetData as $row) {
                $data = new ExcelData();
                $data->setCompteAffaire($row['A']??'');
                $data->setCompteEvenement($row['B']??'');
                $data->setCompteDernierEvenement($row['C']??'');
                $data->setNumeroDeFichier($row['D']??'');
                $data->setLibelleCivilite($row['E']??'');
                $data->setProprietaireActuelDuVehicule($row['F']??'');
                $data->setNom($row['G']??'');
                $data->setPrenom($row['H']??'');
                $data->setNoEtNomDeLaRue($row['I']??'');
                $data->setComplementAdresse1($row['J']??'');
                $data->setCodePostal($row['K']??' ');
                $data->setVille($row['L']??'');
                $data->setTelephone($row['M']??'');
                $data->setTelephonePortable($row['N']??'');
                $data->setTelephoneJob($row['O']??'');
                $data->setEmail($row['P']??'');
                $data->setDateDeMiseEnCirculation($row['Q']??'');
                $data->setDateAchat($row['R']??'');
                $data->setDateDernierEvenement($row['S']??'');
                $data->setLibelleMarque($row['T']??'');
                $data->setLibelleModele($row['U']??'');
                $data->setVersion($row['V']??'');
                $data->setVIN($row['W']??'');
                $data->setImmatriculation($row['X']??'');
                $data->setTypeDeProspect($row['Y']??'');
                $data->setKilometrage($row['Z']??'');
                $data->setLibelleEnergie($row['AA']??'');
                $data->setVendeurVN($row['AB']??'');
                $data->setVendeurVO($row['AC']??'');
                $data->setCommentaireDeFacturation($row['AD']??'');
                $data->setTypeVNVO($row['AE']??'');
                $data->setNoDeDossierVN($row['AF']??'');
                $data->setIntermediareDeVente($row['AG']??'');
                $data->setDateEvenement($row['AH']??'');
                $data->setOrigineEvenement($row['AI']??'');
                $entityManager->persist($data);
                $entityManager->flush();
           }

            return $this->redirectToRoute('app_home');


        }


        return $this->renderForm('import/index.html.twig', [
            'form' => $form,

        ]);
    }
}
