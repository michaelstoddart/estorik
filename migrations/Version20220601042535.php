<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220601042535 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE excel_data (id INT AUTO_INCREMENT NOT NULL, compte_affaire VARCHAR(255) NOT NULL, compte_evenement VARCHAR(255) NOT NULL, compte_dernier_evenement VARCHAR(255) NOT NULL, numero_de_fichier VARCHAR(255) NOT NULL, libelle_civilite VARCHAR(255) NOT NULL, proprietaire_actuel_du_vehicule VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, no_et_nom_de_la_rue VARCHAR(255) NOT NULL, complement_adresse1 VARCHAR(255) NOT NULL, code_postal VARCHAR(255) NOT NULL, ville VARCHAR(255) NOT NULL, telephone VARCHAR(255) NOT NULL, telephone_portable VARCHAR(255) NOT NULL, telephone_job VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, date_de_mise_en_circulation VARCHAR(255) NOT NULL, date_achat VARCHAR(255) NOT NULL, date_dernier_evenement VARCHAR(255) NOT NULL, libelle_marque VARCHAR(255) NOT NULL, libelle_modele VARCHAR(255) NOT NULL, version VARCHAR(255) NOT NULL, vin VARCHAR(255) NOT NULL, immatriculation VARCHAR(255) NOT NULL, type_de_prospect VARCHAR(255) NOT NULL, kilometrage VARCHAR(255) NOT NULL, libelle_energie VARCHAR(255) NOT NULL, vendeur_vn VARCHAR(255) NOT NULL, vendeur_vo VARCHAR(255) NOT NULL, commentaire_de_facturation VARCHAR(255) NOT NULL, type_vnvo VARCHAR(255) NOT NULL, no_de_dossier_vn VARCHAR(255) NOT NULL, intermediare_de_vente VARCHAR(255) NOT NULL, date_evenement VARCHAR(255) NOT NULL, origine_evenement VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE excel_data');
    }
}
